package main // import "server"

import (
	"server/handler"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{"http://localhost:8080"},
		AllowMethods: []string{"GET", "POST", "DELETE", "OPTIONS"},
		AllowHeaders: []string{"*"},
	}))

	r.Use(static.Serve("/", static.LocalFile("./files", true)))

	r.GET("/files", handler.List)

	r.POST("/files", handler.Upload)

	r.DELETE("/files/:uuid", handler.Delete)

	r.GET("/test", handler.ForTest)

	r.Run(":8888")
}
